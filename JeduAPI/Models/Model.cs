﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace JeduAPI.Models
{
    public class JeduContext : DbContext
    {
        public JeduContext(DbContextOptions<JeduContext> options)
            : base(options)
        { }
        public DbSet<DuAn> DuAns { get; set; }
        public DbSet<CongViec> CongViecs { get; set; }

    }
    public class DuAn
    {
        public int Id { get; set; }
        public string Projectname { get; set; }
        public string MaKeHoach { get; set; }
        public string MieuTa { get; set; }
        public int PersionCreatecreate { get; set; }
        public string Details { get; set; }
        public DateTime CreatDate { get; set; }
        public DateTime LastUpdate { get; set; }
    }

    public class CongViec
    {
        public int Id { get; set; }
        public string TenCongViec { get; set; }
        public string MaKeHoach { get; set; }
        public string MieuTa { get; set; }
        public string ChiTiets { get; set; }
        public int NguoiThucHien { get; set; }
        public int NguoiThucHienKhac { get; set; }
        public DateTime CreatDate { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
