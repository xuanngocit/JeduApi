﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace JeduAPI.Migrations
{
    public partial class altercol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CongViecs",
                table: "CongViecs",
                newName: "TenCongViec");

            migrationBuilder.AlterColumn<string>(
                name: "MaKeHoach",
                table: "CongViecs",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TenCongViec",
                table: "CongViecs",
                newName: "CongViecs");

            migrationBuilder.AlterColumn<int>(
                name: "MaKeHoach",
                table: "CongViecs",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
