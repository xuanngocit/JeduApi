﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace JeduAPI.Areas.Admincp.Controllers
{
    [Area("Admincp")]
    [Route("admincp")]
    public class AdmincpController : Controller
    {
        
        public AdmincpController()
        {

        }
        public IActionResult Index()
        {
            return View();
        }
    }
}