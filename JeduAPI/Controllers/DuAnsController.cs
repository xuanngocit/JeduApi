﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JeduAPI.Models;

namespace JeduAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/DuAns")]
    public class DuAnsController : Controller
    {
        private readonly JeduContext _context;

        public DuAnsController(JeduContext context)
        {
            _context = context;
        }

        // GET: api/DuAns
        [HttpGet]
        public IEnumerable<DuAn> GetDuAns()
        {
            return _context.DuAns;
        }

        // GET: api/DuAns/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDuAn([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var duAn = await _context.DuAns.SingleOrDefaultAsync(m => m.Id == id);

            if (duAn == null)
            {
                return NotFound();
            }

            return Ok(duAn);
        }

        // PUT: api/DuAns/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDuAn([FromRoute] int id, [FromBody] DuAn duAn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != duAn.Id)
            {
                return BadRequest();
            }

            _context.Entry(duAn).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DuAnExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DuAns
        [HttpPost]
        public async Task<IActionResult> PostDuAn([FromBody] DuAn duAn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DuAns.Add(duAn);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDuAn", new { id = duAn.Id }, duAn);
        }

        // DELETE: api/DuAns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDuAn([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var duAn = await _context.DuAns.SingleOrDefaultAsync(m => m.Id == id);
            if (duAn == null)
            {
                return NotFound();
            }

            _context.DuAns.Remove(duAn);
            await _context.SaveChangesAsync();

            return Ok(duAn);
        }

        private bool DuAnExists(int id)
        {
            return _context.DuAns.Any(e => e.Id == id);
        }
    }
}