﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JeduAPI.Models;
using Microsoft.AspNetCore.Cors;

namespace JeduAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/CongViecs")]
    [EnableCors("CorsPolicy")]
    public class CongViecsController : Controller
    {
        private readonly JeduContext _context;

        public CongViecsController(JeduContext context)
        {
            _context = context;
        }

        // GET: api/CongViecs
        [HttpGet]
        [EnableCors("CorsPolicy")]
        public IEnumerable<CongViec> GetCongViecs()
        {
            return _context.CongViecs;
        }

        // GET: api/CongViecs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCongViec([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var congViec = await _context.CongViecs.SingleOrDefaultAsync(m => m.Id == id);

            if (congViec == null)
            {
                return NotFound();
            }

            return Ok(congViec);
        }

        // PUT: api/CongViecs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCongViec([FromRoute] int id, [FromBody] CongViec congViec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != congViec.Id)
            {
                return BadRequest();
            }

            _context.Entry(congViec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CongViecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CongViecs
        [HttpPost]
        public async Task<IActionResult> PostCongViec([FromBody] CongViec congViec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CongViecs.Add(congViec);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCongViec", new { id = congViec.Id }, congViec);
        }

        // DELETE: api/CongViecs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCongViec([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var congViec = await _context.CongViecs.SingleOrDefaultAsync(m => m.Id == id);
            if (congViec == null)
            {
                return NotFound();
            }

            _context.CongViecs.Remove(congViec);
            await _context.SaveChangesAsync();

            return Ok(congViec);
        }

        private bool CongViecExists(int id)
        {
            return _context.CongViecs.Any(e => e.Id == id);
        }
    }
}